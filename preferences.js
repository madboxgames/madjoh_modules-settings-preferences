var dependencies = {};
var list = MadJohRequire.getList(dependencies);
define(list, function(require){
	var PreferencesSettings = {
		getBasicPref : function(){
			return {
				user: {
					mail: null,
					firstname: "Guest",
					credits: 50
				},
				credits: 10,
				arcade_save: {},
				token: null
			};
		}
	};

	return PreferencesSettings;
});